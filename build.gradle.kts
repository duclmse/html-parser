import org.gradle.internal.os.OperatingSystem

group = "vn.elite.html.parser"
version = "1.12.2-SNAPSHOT"

plugins {
    java
    jacoco
}

val _os: OperatingSystem = OperatingSystem.current()
println("Building '${rootProject.name}' on ${_os.name} ver ${_os.version}...")

repositories { mavenCentral() }

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

jacoco {
    toolVersion = "0.8.4"
    reportsDir = file("$rootDir/jacoco/reports")
}

dependencies {
    // println("$rootDir/libs/${project.name}")
    // testCompile(fileTree("$rootDir/libs/${project.name}"))

    testCompile("org.junit.jupiter:junit-jupiter:5.5.2")
    testCompile("com.google.code.gson:gson:2.7")
    testCompile("org.eclipse.jetty:jetty-server:9.2.28.v20190418")
    testCompile("org.eclipse.jetty:jetty-servlet:9.2.28.v20190418")
}

val project = this

tasks.register<Copy>("copyLibs") {
    doFirst { println("Copy dependencies of module ${project.name} into $rootDir/libs/${project.name}...") }
    from(configurations.runtime, configurations.testRuntime)
    into("$rootDir/libs/${project.name}")
    doLast { println("Done copying dependencies of module ${project.name}!") }
}

tasks.register<Jar>("sourcesJar") {
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}

//buildDir = File(if (_os.isWindows) "D:/Temp/Compiled/${rootProject.name}/${project.name}" else "build")
println(" - Module ${"%-12s".format(project.name)} -> $buildDir")

//region JACOCO ////////////////////////////////////////////////////////////////////////////////////////////////////////
tasks.test {
    useJUnitPlatform()

    extensions.configure(JacocoTaskExtension::class) {
        setDestinationFile(file("$rootDir/jacoco/jacocoTest.exec"))
        // classDumpDir = file("$rootDir/jacoco/classpathdumps")
    }
    doLast {
        tasks["jacocoTestReport"]
    }
}

tasks.jacocoTestReport {
    reports {
        xml.isEnabled = false
        csv.isEnabled = false
        html.destination = file("$rootDir/jacoco/html")
    }
}

tasks.jacocoTestCoverageVerification {
    violationRules {
        rule {
            limit {
                minimum = "0.5".toBigDecimal()
            }
        }

        rule {
            enabled = false
            element = "CLASS"
            includes = listOf("org.gradle.*")

            limit {
                counter = "LINE"
                value = "TOTALCOUNT"
                maximum = "0.3".toBigDecimal()
            }
        }
    }
}

//endregion JACOCO
